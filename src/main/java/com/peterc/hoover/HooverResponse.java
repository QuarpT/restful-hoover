package com.peterc.hoover;

import java.util.Arrays;

public final class HooverResponse {
    private int[] coords;
    private int patches;

    public HooverResponse(HooverPosition position, int patches) {
        this.coords = position.asArray();
        this.patches = patches;
    }

    public int[] getCoords() {
        return coords;
    }

    public int getPatches() {
        return patches;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        HooverResponse that = (HooverResponse) o;

        if (patches != that.patches) return false;
        return Arrays.equals(coords, that.coords);

    }

    @Override
    public int hashCode() {
        int result = Arrays.hashCode(coords);
        result = 31 * result + patches;
        return result;
    }
}
