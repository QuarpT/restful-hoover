package com.peterc.hoover;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.Map;
import java.util.Set;
import java.util.function.Function;

public class HooverEngine {
    private final HooverPosition roomSize;
    private HooverPosition currentPosition;
    private final Set<HooverPosition> patches;
    private final LinkedList<Character> instructions;

    private static final Map<Character, Function<HooverPosition, HooverPosition>> movementFunctions = new HashMap<>();

    static {
        movementFunctions.put('N', HooverPosition::moveNorth);
        movementFunctions.put('S', HooverPosition::moveSouth);
        movementFunctions.put('E', HooverPosition::moveEast);
        movementFunctions.put('W', HooverPosition::moveWest);
    }

    public HooverEngine(HooverPosition roomSize, HooverPosition currentPosition, Set<HooverPosition> patches, LinkedList<Character> instructions) {
        this.roomSize = roomSize;
        this.currentPosition = currentPosition;
        this.patches = patches;
        this.instructions = instructions;
    }

    public HooverResponse computeHooverResponse() {
        boolean isItemsRemaining;
        int cleanedPatchesCount = 0;

        do {
            if (patches.remove(currentPosition)) {
                cleanedPatchesCount++;
            }

            isItemsRemaining = !instructions.isEmpty();
            if (isItemsRemaining) {
                char nextChar = instructions.removeFirst();
                HooverPosition nextPosition = movementFunctions.get(nextChar).apply(currentPosition);
                if (nextPosition.lessThan(roomSize)) {
                    currentPosition = nextPosition;
                }
            }
        } while (isItemsRemaining);

        return new HooverResponse(currentPosition, cleanedPatchesCount);
    }

}
