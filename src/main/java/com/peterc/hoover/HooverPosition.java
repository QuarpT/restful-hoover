package com.peterc.hoover;

public final class HooverPosition {
    private final int x;
    private final int y;

    public HooverPosition(int x, int y) {
        this.x = x;
        this.y = y;
    }

    public HooverPosition(HooverPosition hooverPosition) {
        x = hooverPosition.x;
        y = hooverPosition.y;
    }

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }

    public boolean lessThan(HooverPosition compare) {
        return x < compare.x && y < compare.y;
    }

    public HooverPosition moveNorth() {
        return new HooverPosition(x, y + 1);
    }

    public HooverPosition moveSouth() {
        return new HooverPosition(x, y > 0 ? y - 1 : y);
    }

    public HooverPosition moveEast() {
        return new HooverPosition(x + 1, y);
    }

    public HooverPosition moveWest() {
        return new HooverPosition(x > 0 ? x - 1 : x, y);
    }

    public int[] asArray() {
        return new int[]{x, y};
    }

    public static HooverPosition fromArray(int[] ar) {
        validateCoordinate(ar);
        return new HooverPosition(ar[0], ar[1]);
    }

    public static void validateCoordinate(int[] ar) {
        boolean isValid = ar != null && ar.length == 2 && ar[0] >= 0 && ar[1] >= 0;
        if (!isValid)
            throw new IllegalArgumentException("Parameter does not represent a hooverable coordinate");
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        HooverPosition that = (HooverPosition) o;

        if (x != that.x) return false;
        return y == that.y;

    }

    @Override
    public int hashCode() {
        int result = x;
        result = 31 * result + y;
        return result;
    }
}
