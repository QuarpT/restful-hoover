package com.peterc.hoover.persistence;

import org.springframework.data.mongodb.repository.MongoRepository;

public interface HooverRepo extends MongoRepository<HooverEntry, String> {
}
