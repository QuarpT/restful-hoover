package com.peterc.hoover.persistence;

import com.peterc.hoover.HooverRequest;
import com.peterc.hoover.HooverResponse;
import org.springframework.data.annotation.Id;

public class HooverEntry {
    @Id
    private String id;

    private HooverRequest request;
    private HooverResponse response;

    public HooverEntry() {}

    public HooverEntry(HooverRequest request, HooverResponse response) {
        this.request = request;
        this.response = response;
    }
}
