package com.peterc.hoover.service;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
public class ClientService {
    @RequestMapping(value = "/", method = RequestMethod.GET)
    public String hooverClient(ModelMap model) throws Exception {
        return "hoover-client";
    }
}
