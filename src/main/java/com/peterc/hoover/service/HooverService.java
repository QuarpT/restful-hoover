package com.peterc.hoover.service;

import com.peterc.hoover.HooverRequest;
import com.peterc.hoover.HooverResponse;
import com.peterc.hoover.persistence.HooverEntry;
import com.peterc.hoover.persistence.HooverRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class HooverService {
    @Autowired
    private HooverRepo repository;

    @RequestMapping(value = "/hoover", method = RequestMethod.POST, consumes = MediaType.ALL_VALUE)
    public HooverResponse hoover(@RequestBody HooverRequest request) throws Exception {
        HooverResponse response = request.validate()
                .createHooverEngine()
                .computeHooverResponse();
        new Thread(() -> repository.save(new HooverEntry(request, response))).start();
        return response;
    }
}
