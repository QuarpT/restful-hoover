package com.peterc.hoover;

import java.util.HashSet;
import java.util.LinkedList;
import java.util.Set;

public final class HooverRequest {

    private static final Set<Character> VALID_INSTRUCTIONS = new HashSet<>();

    static {
        VALID_INSTRUCTIONS.add('N');
        VALID_INSTRUCTIONS.add('E');
        VALID_INSTRUCTIONS.add('S');
        VALID_INSTRUCTIONS.add('W');
    }

    private final int[] roomSize;
    private final int[] coords;
    private final int[][] patches;
    private final String instructions;

    public HooverRequest(int[] roomSize, int[] coords, int[][] patches, String instructions) {
        this.roomSize = roomSize;
        this.coords = coords;
        this.patches = patches;
        this.instructions = instructions;
    }

    public HooverRequest() {
        this.roomSize = null;
        this.coords = null;
        this.patches = null;
        this.instructions = null;
    }

    public int[] getRoomSize() {
        return roomSize;
    }

    public int[] getCoords() {
        return coords;
    }

    public int[][] getPatches() {
        return patches;
    }

    public String getInstructions() {
        return instructions;
    }

    public HooverRequest validate() throws Exception {
        if (patches == null)
        {
            throw new IllegalStateException("Patches member not set");
        }
        HooverPosition.validateCoordinate(roomSize);
        HooverPosition.validateCoordinate(coords);
        for (int[] patch : patches) {
            HooverPosition.validateCoordinate(patch);
        }
        validateDirections(instructions);
        return this;
    }


    static public void validateDirections(String directions) {
        char[] instrChars = directions.toCharArray();
        for (char c : instrChars) {
            if (!VALID_INSTRUCTIONS.contains(c)) {
                throw new IllegalArgumentException("Input instructions are invalid");
            }
        }
    }

    public HooverEngine createHooverEngine() {
        Set<HooverPosition> hooverPatches = new HashSet<>();
        for (int[] patch : patches) {
            hooverPatches.add(HooverPosition.fromArray(patch));
        }
        LinkedList<Character> listInstructions = new LinkedList<>();
        for (char c : instructions.toCharArray()) {
            listInstructions.add(c);
        }
        return new HooverEngine(HooverPosition.fromArray(roomSize), HooverPosition.fromArray(coords), hooverPatches, listInstructions);
    }
}
