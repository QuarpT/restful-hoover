package com.peterc.hoover;

import org.junit.Assert;
import org.junit.Test;

import java.util.HashSet;
import java.util.LinkedList;
import java.util.Set;

public class TestHooverEngine {
    @Test
    public void testComputeResponseWalkIntoWall()
    {
        Set<HooverPosition> patches = new HashSet<>();
        HooverPosition roomSize = new HooverPosition(2, 2);
        patches.add(new HooverPosition(0, 0));
        patches.add(new HooverPosition(1, 1));
        HooverPosition startPosition = new HooverPosition(1, 0);
        HooverResponse expectedResponse = new HooverResponse(new HooverPosition(1, 0), 0);
        String instructions = "EEE";
        computeResponseTestHelper(patches, instructions, roomSize, startPosition, expectedResponse);
    }


    @Test
    public void testComputeResponseWalkNorthEast()
    {
        Set<HooverPosition> patches = new HashSet<>();
        HooverPosition roomSize = new HooverPosition(2, 2);
        patches.add(new HooverPosition(0, 0));
        patches.add(new HooverPosition(1, 1));
        HooverPosition startPosition = new HooverPosition(1, 0);
        HooverResponse expectedResponse = new HooverResponse(new HooverPosition(1, 1), 1);
        String instructions = "EEENNN";
        computeResponseTestHelper(patches, instructions, roomSize, startPosition, expectedResponse);
    }

    @Test
    public void testComputeResponseWalkNorthEastIntoWall()
    {
        Set<HooverPosition> patches = new HashSet<>();
        HooverPosition roomSize = new HooverPosition(2, 2);
        patches.add(new HooverPosition(0, 0));
        patches.add(new HooverPosition(1, 1));
        HooverPosition startPosition = new HooverPosition(1, 0);
        HooverResponse expectedResponse = new HooverResponse(new HooverPosition(1, 1), 1);
        String instructions = "EN";
        computeResponseTestHelper(patches, instructions, roomSize, startPosition, expectedResponse);
    }

    @Test
    public void testComputeResponseWalkNorthSouthWest()
    {
        Set<HooverPosition> patches = new HashSet<>();
        HooverPosition roomSize = new HooverPosition(2, 2);
        patches.add(new HooverPosition(0, 0));
        patches.add(new HooverPosition(1, 1));
        HooverPosition startPosition = new HooverPosition(1, 0);
        HooverResponse expectedResponse = new HooverResponse(new HooverPosition(0, 0), 2);
        String instructions = "NSW";
        computeResponseTestHelper(patches, instructions, roomSize, startPosition, expectedResponse);
    }

    @Test
    public void testComputeResponseCollectAllItemsGoingIntoWall()
    {
        Set<HooverPosition> patches = new HashSet<>();
        HooverPosition roomSize = new HooverPosition(2, 2);
        patches.add(new HooverPosition(0, 0));
        patches.add(new HooverPosition(1, 1));
        HooverPosition startPosition = new HooverPosition(1, 0);
        HooverResponse expectedResponse = new HooverResponse(new HooverPosition(0, 0), 2);
        String instructions = "NNNNEEEESSSSWWWW";
        computeResponseTestHelper(patches, instructions, roomSize, startPosition, expectedResponse);
    }

    @Test
    public void testComputeResponseStationary()
    {
        Set<HooverPosition> patches = new HashSet<>();
        HooverPosition roomSize = new HooverPosition(2, 2);
        patches.add(new HooverPosition(0, 0));
        patches.add(new HooverPosition(1, 1));
        HooverPosition startPosition = new HooverPosition(0, 0);
        HooverResponse expectedResponse = new HooverResponse(new HooverPosition(0, 0), 1);
        String instructions = "NSW";
        computeResponseTestHelper(patches, instructions, roomSize, startPosition, expectedResponse);
    }

    public static void computeResponseTestHelper(Set<HooverPosition> patches, String instructions, HooverPosition roomSize,
                                           HooverPosition startPosition, HooverResponse expectedResponse)
    {
        LinkedList<Character> listInstructions = new LinkedList<>();
        for (char c : instructions.toCharArray()) {
            listInstructions.add(c);
        }
        HooverEngine hooverEngine = new HooverEngine(roomSize, startPosition, patches, listInstructions);
        HooverResponse response = hooverEngine.computeHooverResponse();

        Assert.assertEquals(expectedResponse, response);
    }

    // TODO test more methods, edge cases, validation (I'm doing this in my own time, and it's too sunny outside!)
}
