package com.peterc.hoover;

import org.junit.Assert;
import org.junit.Test;

public class TestHooverRequest {
    @Test
    public void testCreateHooverEngine()
    {
        int[] roomSize = {2, 2};
        int[] coords = {1, 0};
        int[] patch0 = {0, 0};
        int[] patch1 = {1, 1};
        int[][] patches = {patch0, patch1};
        HooverRequest request = new HooverRequest(roomSize, coords, patches, "NNNNEEEESSSSWWWW");
        HooverResponse expectedResponse = new HooverResponse(new HooverPosition(0, 0), 2);
        // An injectable factory could be used by HooverRequest to build HooverEngine.
        // This would enable a mock factory to be injected to capture arguments passed on creation.
        // The arguments passed to the mock factory could then be asserted, enabling HooverRequest to be tested as a Unit
        // However, this would over-complicate the implemented solution for this simple scenario

        Assert.assertEquals(expectedResponse, request.createHooverEngine().computeHooverResponse());
    }

    // TODO test more methods, edge cases, validation (I'm doing this in my own time, and it's too sunny outside!)
}
