package com.peterc.hoover;

import org.junit.Assert;
import org.junit.Test;

import java.util.Arrays;

public class TestHooverPosition {

    @Test
    public void testLessThan()
    {
        Assert.assertFalse(new HooverPosition(5, 2).lessThan(new HooverPosition(6, 2)));
        Assert.assertFalse(new HooverPosition(6, 1).lessThan(new HooverPosition(6, 2)));
        Assert.assertTrue(new HooverPosition(5, 1).lessThan(new HooverPosition(6, 3)));
    }

    @Test
    public void testMovement()
    {
       Assert.assertEquals(new HooverPosition(5, 3), new HooverPosition(5, 2).moveNorth());
       Assert.assertEquals(new HooverPosition(7, 3), new HooverPosition(6, 3).moveEast());
       Assert.assertEquals(new HooverPosition(0, 3), new HooverPosition(0, 3).moveWest());
       Assert.assertEquals(new HooverPosition(5, 3), new HooverPosition(6, 3).moveWest());
       Assert.assertEquals(new HooverPosition(5, 1), new HooverPosition(5, 2).moveSouth());
       Assert.assertEquals(new HooverPosition(5, 0), new HooverPosition(5, 0).moveSouth());
    }

    @Test
    public void testAsArray()
    {
        int[] expectedArray = {5, 2};
        Assert.assertTrue(Arrays.equals(expectedArray, new HooverPosition(5, 2).asArray()));
    }

    // TODO test more methods, edge cases, validation (I'm doing this in my own time, and it's too sunny outside!)
}
