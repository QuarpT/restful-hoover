Requires mongoDB to be running

Uses Spring Boot

To run tests, execute:
$ ./gradlew test

To run web service, execute:
$ ./gradlew bootrun

and navigate to http://localhost:8080

Input json instructions into the higher text-area.
Click submit, to submit json input to the RESTful service.
Output is displayed in the lower output text-area.

To view persisted requests and responses, connect to mongoDB using the mongo client:
$ mongo
> use test
> db.hooverEntry.find()

... will display persisted hoover entries, assuming the test DB is used
